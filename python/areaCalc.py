# areaCalc.py
# This program asks for user input and calculates the area of a circle or a triangle.

option = raw_input("Enter "C" for Circle or "T" for Triangle: ").upper()

if option == "C":
  radius = float(raw_input("Input the radius: "))
  area = 3.14159 + (radius**2)
  print(str("%1.2f") % area)
elif option == "T":
  base = float(raw_input("Input the base: "))
  height = float(raw_input("Input the height: "))
  area = .5 * base * height
  print(str("%1.2f") % area)
else:
  print("Error, you suck.")

print("Thanks for playing!")