# battleShip.py
# Fun little program that places a ship on the board and has the user guess to try and hit it.

from random import randint

# Creates an empty list.
board = []

# Make game board 5 rows with 5 columns. Append to board[].
for x in range(0, 5):
    board.append(["O"] * 5)


def print_board(board):
    for row in board:
        print(" ".join(row))  # Replaces comma with space for board presentation.


print_board(board)

# Random row selection for ship -1 offset so user can guess 1 instead of 0.
def random_row(board):
    return randint(0, len(board) - 1)

# Random column position for ship with -1 offset so user can guess 1 instead of 0.
def random_col(board):
    return randint(0, len(board[0]) - 1)


ship_row = random_row(board)
ship_col = random_col(board)

# Allow the user 4 guesses to sink the ship.
for turn in range(4):
    print("Valid guesses are between 1 and 5.")
    print("Turn", turn + 1)  # Show user the current turn.
    guess_row = int(input("Guess Row: ")) - 1
    guess_col = int(input("Guess Col: ")) - 1

    if guess_row == ship_row and guess_col == ship_col:  # This is a win condition!
        print("Congratulations! You sank my battleship!")
        print()
        break
    else:
        if guess_row not in range(6) or guess_col not in range(6):  # Make sue guesses are on the board.
            print("Oops, that's not even in the ocean.")
        elif board[guess_row][guess_col] == "X":  # Checks if current guess has already been tried.
            print("You guessed that one already.")
            print()
        else:
            print("You missed my battleship!")
            print()
            board[guess_row][guess_col] = "X"  # Marks position on board with an X.
        if turn == 3:  # User has ran out of guesses, end of game.
            print("Correct location was row %d and column %d." % (ship_row, ship_col))
            print("Game Over")
    print_board(board)