# rockPaperScissors.py
"""
This program pits the user against the computer in a game of Rock, Paper, Scissors!
"""

from random import randint

options = ["ROCK", "PAPER", "SCISSORS"]

message = {
  "TIE" : "Yawn, it's a tie...",
  "WON" : "Yay you won!",
  "LOST" : "Aww you lost."
}

def decide_winner(user, computer):
  print("You chose: %s" % user)
  print("The computer chose: %s" % computer)
  
  if user == computer:
    print(message["TIE"])
  elif user == options[0] and computer == options[2]:
    print(message["WON"])
  elif user == options[1] and computer == options[0]:
    print(message["WON"])
  elif user == options[2] and computer == options[1]:
    print(message["WON"])
  else:
    print(message["LOST"])
    
def playRPS():
  user = raw_input("Enter Rock, Paper, or Scissors: ")
  user = user.upper()
  computer = options[randint(0, 2)]
  decide_winner(user, computer)
  
playRPS()